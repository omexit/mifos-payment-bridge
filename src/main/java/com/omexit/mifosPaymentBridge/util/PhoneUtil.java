package com.omexit.mifosPaymentBridge.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aomeri on 9/24/16.
 */
public class PhoneUtil {

    private static PhoneNumberUtil phoneUtil;
    private static Logger logger = LoggerFactory.getLogger(PhoneUtil.class);

    static {
        phoneUtil = PhoneNumberUtil.getInstance();
    }

    public static String formatInternational(String phoneNumber, String regionCode) throws NumberParseException {
        logger.debug("formatInternational({},{})", phoneNumber, regionCode);
        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, regionCode);
        phoneNumber = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        return phoneNumber;
    }

    public static String formatNational(String phoneNumber, String regionCode) throws NumberParseException {
        logger.debug("formatNational({},{})", phoneNumber, regionCode);
        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, regionCode);
        phoneNumber = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
        return phoneNumber;
    }

    public static String formatE164(String phoneNumber, String regionCode) throws NumberParseException {
        logger.debug("formatE164({},{})", phoneNumber, regionCode);
        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, regionCode);
        phoneNumber = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
        return phoneNumber;
    }
}
