package com.omexit.mifosPaymentBridge.util;

/**
 * Created by aomeri on 9/16/16.
 */
public class AppConstants {
    public static final String OUTGOING_PAYMENT_EXCHANGE ="4859886.MIFOS-PAYMENT-BRIDGE.exchange";
    public static final String OUTGOING_PAYMENT_QUEUE ="4859886.MIFOS-PAYMENT-BRIDGE.queue";
    public static final String OUTGOING_PAYMENT_ROUTING_KEY ="4859886.MIFOS-PAYMENT-BRIDGE.routing-key";
}
