package com.omexit.mifosPaymentBridge.config;



import com.omexit.mifosPaymentBridge.auth.privilege.Privilege;
import com.omexit.mifosPaymentBridge.auth.privilege.PrivilegeRepository;
import com.omexit.mifosPaymentBridge.auth.role.Role;
import com.omexit.mifosPaymentBridge.auth.role.RoleRepository;
import com.omexit.mifosPaymentBridge.auth.user.User;
import com.omexit.mifosPaymentBridge.auth.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aomeri on 9/3/16.
 */
@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private boolean alreadySetup = false;

    private final UserService userService;
    private final RoleRepository roleRepository;

    private final PrivilegeRepository privilegeRepository;

    @Autowired
    public InitialDataLoader(PrivilegeRepository privilegeRepository,
                             UserService userService,
                             RoleRepository roleRepository) {
        this.privilegeRepository = privilegeRepository;
        this.userService = userService;
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup)
            return;
        List<Privilege> adminPrivileges = new ArrayList<>();
        Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
        adminPrivileges.add(readPrivilege);
        Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        adminPrivileges.add(writePrivilege);

        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));


        Role adminRole = roleRepository.findByName("ROLE_ADMIN");

        User user=userService.findUserByUsername("mifos");

        if(user==null) {
            user = new User();
            user.setFirstName("Omexit");
            user.setLastName("Mifos");
            user.setUsername("mifos");
            user.setPassword("password");
            user.setEmail("test@test.com");
            user.setRoles(Arrays.asList(adminRole));
            user.setEnabled(true);
            user.setAccountExpired(false);
            user.setCredentialsExpired(false);

            userService.create(user);
        }

        alreadySetup = true;

    }

    @Transactional
    private Privilege createPrivilegeIfNotFound(String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege();
            privilege.setName(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private Role createRoleIfNotFound(String name, List<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
