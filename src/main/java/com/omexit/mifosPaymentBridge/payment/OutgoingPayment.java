package com.omexit.mifosPaymentBridge.payment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.omexit.mifosPaymentBridge.util.DateUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by Antony on 2/9/2016.
 */
@Data
@Entity(name = "outgoing")
@EqualsAndHashCode(callSuper = false)
public class OutgoingPayment extends Payment {

    @JsonIgnore
    @Column(name = "actual_disbursement_date")
    private Date actualDisbursementDate;

    @JsonProperty("actual_disbursement_date")
    private String strActualDisbursementDate;


    public PaymentType getPaymentType() {
        return PaymentType.OUTGOING;
    }

    public String getStrActualDisbursementDate() {
        if (getDateCreated() != null) {
            strActualDisbursementDate = DateUtil.formatDate(getActualDisbursementDate(), DateUtil.DEFAULT_DATE_FORMAT);
        }

        return strActualDisbursementDate;
    }

    @Override
    public String toString() {
        return super.toString() + ", actualDisbursementDate: " + getStrActualDisbursementDate();
    }
}
