package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.omexit.mifosPaymentBridge.channel.ChannelClassificationType;
import com.omexit.mifosPaymentBridge.util.exception.ValidationException;
import com.omexit.mifosPaymentBridge.util.types.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Antony on 2/11/2016.
 */
@Component
public class OutgoingPaymentHandlerFactoryImpl implements OutgoingPaymentHandlerFactory {
    private final BankingPaymentPaymentHandler bankingPaymentPaymentHandler;
    private final EmailPaymentPaymentHandler emailPaymentPaymentHandler;
    private final MobilePaymentPaymentHandler mobilePaymentPaymentHandler;

    @Autowired
    public OutgoingPaymentHandlerFactoryImpl(MobilePaymentPaymentHandler mobilePaymentPaymentHandler,
                                             BankingPaymentPaymentHandler bankingPaymentPaymentHandler, EmailPaymentPaymentHandler emailPaymentPaymentHandler) {
        this.mobilePaymentPaymentHandler = mobilePaymentPaymentHandler;
        this.bankingPaymentPaymentHandler = bankingPaymentPaymentHandler;
        this.emailPaymentPaymentHandler = emailPaymentPaymentHandler;
    }

    public OutgoingPaymentHandler buildPaymentHandler(ChannelClassificationType channelClassificationType) throws ValidationException {
        OutgoingPaymentHandler paymentHandler = null;
        switch (channelClassificationType) {
            case BANKING_CHANNEL:
                paymentHandler = bankingPaymentPaymentHandler;
                break;
            case EMAIL_MONEY_CHANNEL:
                paymentHandler = emailPaymentPaymentHandler;
                break;
            case MOBILE_MONEY_CHANNEL:
                paymentHandler = mobilePaymentPaymentHandler;
                break;
            default:
                throw new ValidationException("Invalid channel type", "Not a valid ChannelClassificationType, refer to javadocs", ErrorCode.IVALID_CHANNEL_TYPE);
        }
        return paymentHandler;
    }
}
