package com.omexit.mifosPaymentBridge.payment.outgoing.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Antony on 2/18/2016.
 */
@Data
public class PaymentCallback {
    @JsonProperty("payment_bridge_ref")
    private Long paymentBridgeRef;
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("request_timestamp")
    private String requestTimestamp;
}
