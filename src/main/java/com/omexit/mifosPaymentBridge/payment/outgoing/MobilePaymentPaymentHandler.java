package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.i18n.phonenumbers.NumberParseException;
import com.omexit.mifosPaymentBridge.externalApi.ExternalApiServiceGenerator;
import com.omexit.mifosPaymentBridge.externalApi.mifos.MifosApiInterface;
import com.omexit.mifosPaymentBridge.mifos.portfolio.client.Client;
import com.omexit.mifosPaymentBridge.payment.*;
import com.omexit.mifosPaymentBridge.util.PhoneUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Antony on 2/11/2016.
 */
@Service
public class MobilePaymentPaymentHandler implements OutgoingPaymentHandler {

    private final PaymentService paymentService;

    private final ExternalApiServiceGenerator externalApiServiceGenerator;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    private CachingConnectionFactory connectionFactory;
    private AmqpAdmin amqpAdmin;
    private ObjectMapper jacksonObjectMapper;

    @Value("${mifosPaymentBridge.processPayment.regionCode}")
    private String regionCode;
    @Value("${mifosPaymentBridge.mifos.baseUrl}")
    private String mifosBaseUrl;
    @Value("${mifosPaymentBridge.mifos.username}")
    private String mifosUsername;
    @Value("${mifosPaymentBridge.mifos.password}")
    private String mifosPassword;

    @Autowired
    public MobilePaymentPaymentHandler(PaymentService paymentService,
                                       ExternalApiServiceGenerator externalApiServiceGenerator) {
        this.paymentService = paymentService;

        connectionFactory = new CachingConnectionFactory();
        amqpAdmin = new RabbitAdmin(connectionFactory);
        jacksonObjectMapper = new ObjectMapper();
        this.externalApiServiceGenerator = externalApiServiceGenerator;
    }


    @Transactional
    @Override
    public void processPayment(OutgoingPayment outgoingPayment) {
        int channelRetryCountMax = outgoingPayment.getChannel().getMaxRetryCount();
        try {
            MifosApiInterface mifosApiService =
                    externalApiServiceGenerator.createService(MifosApiInterface.class, mifosBaseUrl, mifosUsername, mifosPassword);

            if (outgoingPayment.getRetryCount() <= channelRetryCountMax) {
                outgoingPayment.setRetryCount(outgoingPayment.getRetryCount() + 1);

                Map<String, String> clientQueryParams = new HashMap<>();
                clientQueryParams.put("tenantIdentifier", outgoingPayment.getTenantId());
                clientQueryParams.put("pretty", "false");
                clientQueryParams.put("fields", "mobileNo");


                logger.info("Mifos.getClient({},{})", outgoingPayment.getClientId(), clientQueryParams);
                //Get Client details from mifos
                Call<Client> clientCall = mifosApiService.getClient(outgoingPayment.getClientId(), clientQueryParams);
                Response<Client> clientResponse = clientCall.execute();
                logger.info(String.format("getClient() - response {:isSuccess %s :statusCode %s, :message %s}",
                        clientResponse.isSuccessful(), clientResponse.code(), clientResponse.message()));

                if (clientResponse.isSuccessful()) {
                    Client client = clientResponse.body();

                    //Format phone number
                    String phoneNumber = PhoneUtil.formatE164(client.getMobileNo(), regionCode);

                    outgoingPayment.setPaymentAccount(phoneNumber);
                    outgoingPayment.setDescription("Mobile money payment to: " + phoneNumber);
                    outgoingPayment.setPaymentStatus(PaymentStatusType.PAYMENT_PROCESSING);

                    String queueName = outgoingPayment.getChannel().getChannelName();

                    //Declare channel in rabbitmq just in case
                    amqpAdmin.declareQueue(new Queue(queueName));

                    AmqpTemplate template = new RabbitTemplate(connectionFactory);
                    template.convertAndSend(queueName, jacksonObjectMapper.writeValueAsString(outgoingPayment));

//                    String foo = (String) template.receiveAndConvert(queueName);
//
//                    System.err.println("Recieved: - " + foo);

                    logger.info(String.format("Push Request to Queue(%s) -> requestID(%s) account: %s, amount: %s",
                            outgoingPayment.getChannel().getChannelName(),
                            outgoingPayment.getId(),
                            outgoingPayment.getPaymentAccount(),
                            outgoingPayment.getTransactionAmount()));


                    outgoingPayment = (OutgoingPayment) paymentService.saveOrUpdatePayment(outgoingPayment);
                }
            } else {
                logger.warn(String.format("Mobile money request ->(%s) failed. Request exceeded maximum re-tries of %s!", outgoingPayment.getId(), outgoingPayment.getRetryCount()));
                outgoingPayment.setPaymentStatus(PaymentStatusType.PAYMENT_FAILED);
                outgoingPayment.setStatusReasonCodeMessage(String.format("Request exceeded maximum re-tries of: %s", outgoingPayment.getRetryCount()));
                paymentService.saveOrUpdatePayment(outgoingPayment);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } catch (NumberParseException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
