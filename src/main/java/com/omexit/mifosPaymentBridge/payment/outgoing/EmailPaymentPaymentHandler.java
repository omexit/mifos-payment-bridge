package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.omexit.mifosPaymentBridge.payment.OutgoingPayment;
import com.omexit.mifosPaymentBridge.payment.PaymentService;
import org.springframework.stereotype.Service;

/**
 * Created by Antony on 2/11/2016.
 */
@Service
public class EmailPaymentPaymentHandler implements OutgoingPaymentHandler {

    private PaymentService paymentService;

    public EmailPaymentPaymentHandler(PaymentService paymentService) {
        this.paymentService=paymentService;
    }

    @Override
    public void processPayment(OutgoingPayment outgoingPayment) {
    }
}
