package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.omexit.mifosPaymentBridge.payment.OutgoingPayment;
import com.omexit.mifosPaymentBridge.payment.Payment;
import com.omexit.mifosPaymentBridge.payment.PaymentService;
import com.omexit.mifosPaymentBridge.payment.PaymentStatusType;
import com.omexit.mifosPaymentBridge.util.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by aomeri on 9/15/16.
 */
@Service
public class OutgoingServiceImpl implements OutgoingService {

    private final PaymentService paymentService;
    private final OutgoingPaymentHandlerFactory outgoingPaymentHandlerFactory;
    private Logger logger = LoggerFactory.getLogger(this.getClass());



    @Autowired
    public OutgoingServiceImpl(PaymentService paymentService, OutgoingPaymentHandlerFactory outgoingPaymentHandlerFactory) {
        this.paymentService = paymentService;
        this.outgoingPaymentHandlerFactory = outgoingPaymentHandlerFactory;
    }

    @Scheduled(fixedDelayString = "${mifosPaymentBridge.processPayment.fixedDelay:2000}", initialDelay = 5000)
    @Override
    public void processOutgoingPayment() {
        List<Payment> payments = paymentService.findTransactionsToProcess(new Date(),
                PaymentStatusType.PAYMENT_PENDING);
        if (!payments.isEmpty()) {

            logger.info("Fetched: " + payments.size() + " outgoing records to process");

            OutgoingPaymentHandler handler = null;

            for (Payment payment : payments) {
                try {
                    handler = outgoingPaymentHandlerFactory.buildPaymentHandler(payment.getChannel().getChannelType());
                    handler.processPayment((OutgoingPayment) payment);
                } catch (ValidationException e) {
                    logger.error(e.getDeveloperMessage(), e);
                }
            }

        }

    }
}
