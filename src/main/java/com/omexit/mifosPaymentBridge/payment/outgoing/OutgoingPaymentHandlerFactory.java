package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.omexit.mifosPaymentBridge.channel.ChannelClassificationType;
import com.omexit.mifosPaymentBridge.payment.PaymentService;
import com.omexit.mifosPaymentBridge.util.exception.ValidationException;

/**
 * Created by aomeri on 9/15/16.
 */
public interface OutgoingPaymentHandlerFactory {
    OutgoingPaymentHandler buildPaymentHandler(ChannelClassificationType channelClassificationType) throws ValidationException;
}
