package com.omexit.mifosPaymentBridge.payment.outgoing.api;

import com.omexit.mifosPaymentBridge.util.HttpClientHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class OutgoingPaymentApiClient {
    private static Retrofit retrofit = null;
    private static Logger logger = LoggerFactory.getLogger(OutgoingPaymentApiClient.class);

    public static OutgoingPaymentApiInterface getClient(String channelUrl) {
        logger.info("Initializing OutgoingPaymentApiClient ...");
        logger.debug("MifosApiClient - [channelUrl: {}]", channelUrl);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(channelUrl)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .client(HttpClientHelper.getUnsafeOkHttpClient())
                    .build();
        }
        return retrofit.create(OutgoingPaymentApiInterface.class);
    }
}