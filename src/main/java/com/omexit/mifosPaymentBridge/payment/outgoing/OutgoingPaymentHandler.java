package com.omexit.mifosPaymentBridge.payment.outgoing;

import com.omexit.mifosPaymentBridge.payment.OutgoingPayment;

/**
 * Created by Antony on 2/11/2016.
 */
public interface OutgoingPaymentHandler {
    void processPayment(OutgoingPayment outgoingPayment);
}
