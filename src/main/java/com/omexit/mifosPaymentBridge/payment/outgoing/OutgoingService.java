package com.omexit.mifosPaymentBridge.payment.outgoing;

/**
 * Created by aomeri on 9/15/16.
 */
public interface OutgoingService {
    void processOutgoingPayment();
}
