package com.omexit.mifosPaymentBridge.payment.outgoing.api;

import com.omexit.mifosPaymentBridge.payment.outgoing.domain.PaymentAck;
import com.omexit.mifosPaymentBridge.payment.outgoing.domain.PaymentRequest;
import com.omexit.mifosPaymentBridge.payment.outgoing.domain.PaymentCallback;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Antony on 4/5/2016.
 */
public interface OutgoingPaymentApiInterface {
    @POST("./")
    Call<PaymentCallback> postPaymentRequest(@Body PaymentRequest request);

    @POST("./")
    Call<Void> postCallbackACK(@Body PaymentAck request);
}
