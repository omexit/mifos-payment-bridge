package com.omexit.mifosPaymentBridge.payment;

/**
 * Created by Antony on 2/11/2016.
 */
public enum PaymentType {
    INCOMING, OUTGOING
}
