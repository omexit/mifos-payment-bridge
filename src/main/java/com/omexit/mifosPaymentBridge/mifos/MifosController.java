package com.omexit.mifosPaymentBridge.mifos;

import com.omexit.mifosPaymentBridge.mifos.hook.HookHandler;
import com.omexit.mifosPaymentBridge.mifos.hook.HookHandlerFactory;
import com.omexit.mifosPaymentBridge.util.BaseController;
import com.omexit.mifosPaymentBridge.util.exception.UnknownRequestException;
import com.omexit.mifosPaymentBridge.util.exception.ValidationException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Antony on 2/9/2016.
 */
@RestController
public class MifosController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final HookHandlerFactory hookHandlerFactory;

    @Autowired
    public MifosController(HookHandlerFactory hookHandlerFactory) {
        this.hookHandlerFactory = hookHandlerFactory;
    }

    @RequestMapping(value = PAYMENT_HOOK, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> paymentHook(@RequestHeader("Fineract-Platform-TenantId") final String tenantId,
                                            @RequestHeader("X-Fineract-Entity") final String entity,
                                            @RequestHeader("X-Fineract-Action") final String action,
                                            @RequestBody final String payload) throws ValidationException, UnknownRequestException {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        logger.info(String.format("paymentHook(tenantId=%s,entity=%s,action=%s,payload=%s)", tenantId, entity, action, payload));
        //Add missing fields from the headers to the payload
        JSONObject jsonObject = new JSONObject(payload);
        jsonObject.put("entity", entity);
        jsonObject.put("action", action);
        jsonObject.put("tenantId", tenantId);
        String request = jsonObject.toString();

        HookHandler hookHandler =  hookHandlerFactory.buildHookHandler(entity);
        hookHandler.handleHook(request);

        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }


    @Bean
    public TaskExecutor hookThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.setQueueCapacity(100);

        return taskExecutor;
    }
}
