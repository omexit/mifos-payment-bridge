package com.omexit.mifosPaymentBridge.mifos.hook;

import com.omexit.mifosPaymentBridge.util.exception.UnknownRequestException;

/**
 * Created by aomeri on 9/3/16.
 */
public interface HookHandlerFactory {
    HookHandler buildHookHandler(String entity) throws UnknownRequestException;
}
