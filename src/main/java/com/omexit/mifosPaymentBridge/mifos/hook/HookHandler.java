package com.omexit.mifosPaymentBridge.mifos.hook;


import com.omexit.mifosPaymentBridge.util.exception.ValidationException;

/**
 * Created by Antony on 5/27/2016.
 */
public interface HookHandler {
    void handleHook(String request) throws ValidationException;
}
