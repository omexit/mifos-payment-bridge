package com.omexit.mifosPaymentBridge.auth.privilege;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by aomeri on 9/3/16.
 */
@Data
@Entity(name = "tbl_privileges")
public class Privilege  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

}