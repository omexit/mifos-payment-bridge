package com.omexit.mifosPaymentBridge.auth.privilege;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by aomeri on 9/3/16.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
    Privilege findByName(String name);
}
