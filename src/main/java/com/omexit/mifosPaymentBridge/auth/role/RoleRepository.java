package com.omexit.mifosPaymentBridge.auth.role;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by aomeri on 9/3/16.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
